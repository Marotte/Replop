#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys

try:

    sys.path.insert(0, './src')
    import re
    from datetime import datetime

except ImportError as e:

    print(str(e), file=sys.stderr)
    print('Cannot find the module(s) listed above. Exiting.', file=sys.stderr)
    sys.exit(1)


class Norloge:

    s_internal   = '19700101010000'
    s_human      = '1970-01-01 01:00:00'
    s_short      = '01:00:00'
    
    r_internal = re.compile(r'^\d{14}$')
    r_human    = re.compile(r'^\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}$')
    r_short    = re.compile(r'^\d{2}:\d{2}:\d{2}$')
    
    unix_ts    = 0

    def unix(self, s):
        """Return seconds since the Epoch from an internal norloge representation"""

        try:
            
            if not self.r_internal.match(s):
                
                return False 
            
            self.unix_ts = datetime(*map(int, (s[0:4], s[4:6], s[6:8], s[8:10], s[10:12], s[12:14]))).strftime('%s')
            return self.unix_ts
            
        except Exception as e:
            
            print(str(e),file=sys.stderr)
            return False

    def human(self, s):
    
        try:
            
            if not self.r_internal.match(s):
                
                return False
            
            self.s_human = s[0:4]+'-'+s[4:6]+'-'+s[6:8]+' '+s[8:10]+':'+s[10:12]+':'+s[12:14]
            return self.s_human
            
        except Exception as e:
            
            print(str(e),file=sys.stderr)
            return False

    def short(self, s):
    
        try:
            
            if not self.r_internal.match(s):
                
                return False
            
            self.s_short = s[8:10]+':'+s[10:12]+':'+s[12:14]
            return self.s_short
            
        except Exception as e:
            
            print(str(e),file=sys.stderr)
            return False
            
    def internal(self, s):

        try:
            
            if not self.r_human.match(s):
                
                return False
    
            _s = s.split(' ')
            date = _s[0].split('-')
            time = _s[1].split(':')
            self.s_internal = date[0]+date[1]+date[2]+time[0]+time[1]+time[2]
            return self.s_internal
        
        except Exception as e:
            
            print(str(e), file=sys.stderr)
            return False

    def today(self, s):
        """Return an internal representation from a short representation, assuming it belong to today or yesterday"""
        
        try:
        
            if not self.r_short.match(s):
                
                return False

            now = datetime.now()
            l_short = s.split(':')
            d_horloge = datetime(now.year, now.month, now.day, int(l_short[0]), int(l_short[1]), int(l_short[2]))
            if (d_horloge < now):
                self.s_internal = datetime.now().strftime('%Y%m%d')+s.replace(':','')
            else:
                d_horloge = datetime(now.year, now.month, now.day - 1, int(l_short[0]), int(l_short[1]), int(l_short[2]))
                self.s_internal = d_horloge.strftime('%Y%m%d%H%M%S')
            return self.s_internal

        except Exception as e:
            
            print(str(e), file=sys.stderr)
            return False        


if (__name__ == '__main__'):
    
    norloge = Norloge()
    
    try:

        print(getattr(norloge,sys.argv[1])(*sys.argv[2:]))
        sys.exit(0)

    except Exception as e:
            
        print(e, file=sys.stderr)
            
        try: 

            print(getattr(norloge,sys.argv[1])())
            sys.exit(0)
            
        except Exception as e:
            
            print(e, file=sys.stderr)
            print("Nothing to do.", file=sys.stderr)
            sys.exit(0)
    
    
