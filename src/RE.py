#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

import sys

try:

    sys.path.insert(0, './src')
    import re

except ImportError as e:

    print(str(e), file=sys.stderr)
    print('Cannot find the module(s) listed above. Exiting.', file=sys.stderr)
    sys.exit(1)

class RE:

    def __init__(self):
    
        self.regexSrc = []
    
        try:
        
            with open('regex','r') as re_file:
                ln = 1
                for line in re_file:
                    if len(line) < 2: continue
                    self.regexSrc.append((line.strip(),ln))
                    ln += 1
                
        except Exception as e:
            
            print('No regular expression to load: '+str(e), file=sys.stderr)

    def getRESrc(self):
        
        return self.regexSrc

    def setRE(self, regex):
    
        self.regex = []
    
        for r in regex:
            
            try: self.regex.append((r[0],re.compile(r[0]),r[1]))
            except Exception as e: print('`'+r[0]+'` is not a valid regular expression: '+str(e))
            
    def getRE(self):

        return self.regex

    def match(self, string):

        for r in self.regex:
            # ~ print('RE:'+r[0]+' STRING:'+string, end='')
            if r[1].match(string):
                # ~ print(' MATCH')
                yield r[0]
            # ~ else: print()
        
