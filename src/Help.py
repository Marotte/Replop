"""Replop help"""

import sys

def man_general(what = sys.argv[0]):
    """General help"""
    return """
Replop. Regular expression based talking bot.

Usage: """+what+""" <command> [arguments …]

Commands:

    -h/help        :           show this help and exit.
    -u/update      :           update post database

To show help on a particular command, use `"""+what+""" help <command>`.

Example: """+what+""" help update
"""

def man_update():
    """Show update help"""
    return """
Update the database, getting posts from the given backend URL (XML or TSV).

Example: plop update http://exemple.com/backend.tsv
"""

def man_watch():
    """Show watch help"""
    return """
Continuously update the database, getting posts from the given backend URL (XML or TSV), every N seconds.

Example: plop watch http://exemple.com/backend.tsv 30
"""

def man_process():
    """Show watch help"""
    return """
Process the posts in database :

 - find relation between posts from norloge usage

Example: plop process
"""

def man_reset_post_status():
    """Reset post status help"""
    return """
Reset post status to 'NEW' for all post in database.

Example: plop reset-post-status
"""

def man(section = 'general'):
    """Show help"""
    if section == 'general': print(man_general())
    elif section == 'update': print(man_update())
    elif section == 'watch': print(man_watch())
    elif section == 'process': print(man_process())
    elif section == 'reset-post-status': print(man_reset_post_status())
    else:
        print("\nNo help section named \""+section+"\"")
        print(man_general())
        sys.exit(2)
        

