#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

__version__ = "0.1"
__author__  = "Marotte"
__licence__ = "GPL"
__status__  = "Experimental"

import sys

try:

    sys.path.insert(0, './src')
    import os
    import time
    import string
    import random
    import sqlite3
    import requests
    import xml.etree.ElementTree as etree
    import html
    from hashlib import md5
    
    import Norloge
    import RE

except ImportError as e:

    print(str(e), file=sys.stderr)
    print('Cannot find the module(s) listed above. Exiting.', file=sys.stderr)
    sys.exit(1)

class Replop:
    """Replop"""
    
    cycle_time    = [60]     # Get posts every N seconds
    posts         = []       # Posts
    proxies       = {'http':os.getenv('HTTP_PROXY')}

    def __init__(self, name  = 'Replop',
                 logname     = 'zir',
                 post_url    = 'https://linuxfr.org/board',
                 cookie_name = 'linuxfr.org_session',
                 cookie_file = './cookie'):
        
        self.name      = name+' '+''.join(random.SystemRandom().choice(string.hexdigits) for _ in range(8)).upper()
        self.logname   = logname

        try: cookie = open(cookie_file).read()
        except Exception: cookie = ''

        self.connection    = sqlite3.connect(name+'.sqlite')
        self.cursor        = self.connection.cursor()
        self.post_url      = post_url
        self.cursor.execute('CREATE TABLE IF NOT EXISTS post (id INT, time INT, message TEXT, login TEXT, info TEXT, status TEXT, PRIMARY KEY(id))')
        self.cursor.execute('CREATE TABLE IF NOT EXISTS relation (question INT, answer INT, status TEXT, FOREIGN KEY(question) REFERENCES post(id), FOREIGN KEY(answer) REFERENCES post(id))')
        self.cursor.execute('CREATE TABLE IF NOT EXISTS re (string TEXT, priority INT, PRIMARY KEY(string))')
        self.cursor.execute('CREATE TABLE IF NOT EXISTS re_relation (question TEXT, answer TEXT, freq INT, FOREIGN KEY(question) REFERENCES re(string), FOREIGN KEY(answer) REFERENCES re(string), PRIMARY KEY(question,answer))')
        self.headers = {'Content-Type':'application/x-www-form-urlencoded;',  'Referer': post_url, 'User-Agent': 'Zir'}
        self.cookies = cookies = {'https':'1', cookie_name:cookie.rstrip()}
        
        self.loadRE()

    def loadRE(self):
        
        self.rem = RE.RE()
        regex_sources = self.rem.getRESrc() + self.cursor.execute('SELECT string, priority FROM re').fetchall()
        self.rem.setRE(list(set(regex_sources)))
        self.cursor.executemany('INSERT OR REPLACE INTO re (string, priority) VALUES (?,?)', regex_sources)
        print(str(len(self.rem.getRE()))+' regex loaded.', file=sys.stderr)
        self.connection.commit()

    def post(self, post, horloge = '', login = ''):
        """POST message to the backend."""
        post = html.unescape(post)
        if (post):
            print(self.name+" post('"+horloge+' '+post+' '+login+"')", file=sys.stderr)
            f = requests.post(self.post_url, headers=self.headers, cookies=self.cookies, proxies=self.proxies, data={"board[message]":horloge+' '+post+' '+login, "message":horloge+' '+post+' '+login})
            return True
        return False
    
    def __del__(self):

        self.connection.close()

    def get_posts(self, url):
        """HTTP GET Request the backend and insert posts to the database with status 'NEW'."""

        headers = {"User-Agent": "", "Accept": "application/xml"}
        self.get_url = url
        session = requests.Session()
        
        try:
            response = session.get(url, headers=headers, proxies=self.proxies)
        except (requests.exceptions.ConnectionError,
                requests.exceptions.MissingSchema) as e:
            print(self.name+" "+str(e), file=sys.stderr)
            return False
        
        if response.headers['content-type'] in ['text/xml; charset=utf-8', 'application/xml']:
            
            try:  
                tribune = etree.fromstring(response.text)
            except etree.ParseError as error:
                return False
                
            for post in tribune:
                values =  (post.attrib['id'], post.attrib['time'], post[1].text, post[2].text, post[0].text, 'NEW')
                self.cursor.execute("INSERT OR IGNORE INTO post VALUES (?, ?, ?, ?, ?, ?)", values)
            self.connection.commit()
            return True

        elif response.headers['content-type'] in ['text/plain; charset=utf-8','text/tab-separated-values; charset=utf-8']:
            
            try:
                
                tribune = response.text.split('\n')
                
                for post in tribune:
                    
                    if not post: continue
                    
                    l_post = post.split('\t')
                    message = l_post[4]
                    self.cursor.execute("INSERT OR IGNORE INTO post VALUES (?, ?, ?, ?, ?, ?)", [l_post[0],l_post[1],message,l_post[3],l_post[2],'NEW'])
                
                self.connection.commit()
                return True
                
            except Exception as e:
                
                print(str(e), file=sys.stderr)
                return False 

        else:
            
            print('Wrong content-type!', file=sys.stderr)
            return False

    def update(self, url):
        """Call get_posts(url)."""
        print('Getting posts from backend '+url, file=sys.stderr)
        if not self.get_posts(url): sys.exit(4)
    
       
    def watch(self, url, cycle_time = []):
        """Watch the backend for new posts."""
        
        self.get_url = url
        if cycle_time == []: cycle_time = self.cycle_time
        if int(cycle_time[0]) < 0: cycle_time = self.cycle_time
        print('Watching backend '+url+' every '+str(cycle_time[0])+' seconds', file=sys.stderr)
        while True:
            
            try:
                self.update(url)
                time.sleep(int(cycle_time[0]))
                
            except KeyboardInterrupt:

                print('KeyboardInterrupt',file=sys.stderr)
                sys.exit(0)

    def get_relations(self):
        
        norloge = Norloge.Norloge()
        new_posts = self.cursor.execute('SELECT time,message,id FROM post WHERE status = "NEW"').fetchall()
        print('Getting relations between posts for '+str(len(new_posts))+' posts', file=sys.stderr)
        for post in new_posts:

            first_word = post[1].split()[0]
            target_norloge = norloge.today(first_word)
            if target_norloge:
                try:
                    target_post_id = self.cursor.execute('SELECT id FROM post WHERE time = ?', (target_norloge,)).fetchone()[0]
                    self.cursor.execute('INSERT OR REPLACE INTO relation (question,answer,status) VALUES (?,?,?)',(target_norloge,post[0],'NEW'))
                    self.cursor.execute('UPDATE post SET status = ? WHERE id = ?', ('RELATED', post[2]))
                except TypeError: continue # This post has no answer yet

        self.connection.commit()   
            
    def process(self):
        
        self.get_relations()

    def getMessages(self):
        
        for record in self.cursor.execute('SELECT message,time FROM post').fetchall():
            
            yield record[0],record[1]

    def getAnswers(self, message_time):

        return self.cursor.execute('SELECT message,time FROM post INNER JOIN relation ON relation.answer = post.time WHERE relation.question = ? AND relation.status = ?', (message_time,'NEW')).fetchall()

    def getMessage(self, message_time):
        
        return self.cursor.execute('SELECT message,time FROM post WHERE time = ?', (message_time,)).fetchall()

    def setRERelations(self):
        
        for m,message_time in self.getMessages():
            # ~ print('MESSAGE:',m,message_time)
            answers = self.getAnswers(message_time)
            if not answers: continue
            # ~ print('NUMBER OF ANSWERS:',len(answers))
            for a in answers:
                # ~ print('ANSWER:',a[0])
                for item1 in self.rem.match(' '.join(a[0].split(' ')[1:])):
                    # ~ print('MATCHING ITEM:',item1)
                    for item2 in self.rem.match(m):
                        if item2:
                            print('MATCHING MESSAGE:',m)
                            print('ANSWER          :',a[0])
                            print(item1,' → ',item2)
                            self.cursor.execute('INSERT OR IGNORE INTO re_relation (question,answer,freq) VALUES (?,?,?)',(item2,item1,0))
                            self.cursor.execute('UPDATE re_relation SET freq = freq + 1 WHERE question = ? AND answer = ?', (item2,item1))
                            self.cursor.execute('UPDATE relation SET status = ? WHERE answer = ?',('KNONW', a[1]))
                            self.connection.commit()


    def resetPostStatus(self):
        
        self.cursor.execute('UPDATE post SET status = "NEW"')
        self.connection.commit()
        print('All post status set back to "NEW"', file=sys.stderr)
            

if (__name__ == '__main__'):
    
    print('Running from the module itself. Exiting.')
    exit(127)

